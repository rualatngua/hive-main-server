var V = require('./validate');

var Validator = {};

Validator.postAauthLocal = function(req, res, next) {
    V.chain([
        V.pickOnly(req, 'body', ['email', 'password']),
        V.checkTypes(req.body, {email: String, password: String}),
        V.mustHaveKeys(req.body, ['email', 'password']),
        V.sanitize(req.body, ['email', 'password'])
    ], next);
};

Validator.postPwdrecover = function(req, res, next) {
    V.chain([
        V.pickOnly(req, 'body', ['email']),
        V.checkTypes(req.body, {email: String}),
        V.mustHaveKeys(req.body, ['email']),
        V.sanitize(req.body, ['email'])
    ], next);
};

Validator.postPwdreset = function(req, res, next) {
    V.chain([
        V.pickOnly(req, 'body', ['link', 'password']),
        V.checkTypes(req.body, {link: String, password: String}),
        V.mustHaveKeys(req.body, ['link', 'password']),
        V.sanitize(req.body, ['link', 'password'])
    ], next);
};

module.exports = Validator;