var geoip = require('geoip');

var Country = geoip.Country;
var path = require('path');
var country = new Country(path.resolve(__dirname, 'GeoIP.dat'));

module.exports = {
    /**
     * Get country by specified ip address.
     * @param ip - client ip address
     * @returns {String} - 2-chars country code.
     */
    getByIp: function(ip) {
        if(!ip) return null;
        var country_obj = country.lookupSync(ip);
        return country_obj ? country_obj.country_code : null;
    }
};