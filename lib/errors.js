var _ = require('lodash');
var moment = require('moment');

/*
 * Merge all error modules in single place.
 */
var errors = _.extend({},
    require('./errors_common'),
    require('./errors_user'),
    require('./errors_mailer'),
    require('./errors_hashlink')
);

/*
 * Check errors for possible duplicates of 'code' property.
 * Will throw exception with array of found duplicates.
 */
var errorsArray = _.toArray(errors);
var duplicates = _.difference(errorsArray, _.uniq(errorsArray, 'code'));
if(duplicates.length) {
    var message = JSON.stringify({
        info: "Look 'duplicates' Array for list of all found duplicates",
        duplicates: duplicates
    });

    throw {
        name: "Duplicated error codes are found in './lib/errors' module",
        message: message
    };
}

/*
 * Make functions with ability to extend default error message
 * (add some new fields or rewrite existed).
 */
var errorFunctions = {};
_.forEach(errors, function(value, key) {
    errorFunctions[key] = function(data) {
        data = data ? data : {};
        data.timestamp = moment(new Date()).format('YYYY-MM-DD HH:MM:ss.S');
        return _.extend(value, data);
    };
});

module.exports = errorFunctions;