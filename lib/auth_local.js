var log      = require("./logger").getLogger('auth_local');
var passport = require("passport");
var settings = require('./settings');

var LocalStrategy = require("passport-local").Strategy;
var User          = require('./user');
var errors        = require('./errors');
var Validator     = require('./validate_api_auth');

var express = require("express");
var app = module.exports = express();

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, function(email, password, done) {
    User.getOne({email: email})
        .then(function(user) {
            log.debug("get user from db by email", user);
            if(!user) {
                log.debug("no user found with such email", email);
                return done(errors.NO_USER_WITH_EMAIL(), false);
            }
            if(!user.authenticate(password)) {
                log.debug("not valid pair email/password", email);
                return done(errors.NOT_VALID_EMAIL_OR_PASSWORD(), false);
            }
            log.debug("user is successfully authenticated", email);
            return done(null, user);
        })
        .catch(function(err) {
            log.error("error on fetching user from db", err);
            return done(err);
        });
}));

/* POST /auth/local Login with email/login and password */
app.post('/auth/local', Validator.postAauthLocal, function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
        if(err) {
            log.error("error in passport.authenticate", err);
            return next(err);
        }
        if(!user) {
            log.debug("user authentication faild, no such user");
            return next(errors.USER_AUTH_FAILD());
        }

        req.logIn(user, function(err) {
            if(err) {
                log.debug("user login faild", err);
                return next(errors.USER_LOGIN_FAILD());
            }
            return next();
        });
    })(req, res, next);
}, function(req, res, next) {
    log.debug("post /auth/local", {isAuth: req.isAuthenticated(), user: req.user});

    req.session.cookie.expires = new Date(Date.now() + settings.config.session_max_age);

    res.data = {accessToken: req.sessionID};

    next();
});
