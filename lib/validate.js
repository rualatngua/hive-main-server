var _ = require('lodash');
var Q = require('q');
var log = require('./logger').getLogger('validate');
var errors = require('./errors');
var should = require('should');
var maybe  = require('maybe2');

var Validate = {};

/**
 * Accepts array of validation promises, and call next() with error if any
 * @param validators
 * @param next
 */
Validate.chain = function(validators, next) {
    Q.all(validators)
        .done(function() {
            return next();
        }, function(error) {
            log.warn('validation error', error);
            return next(error);
        });
};

/**
 * Get only restricted properties from req body
 * @param obj
 * @param property
 * @param keys
 * @returns {exports|*}
 */
Validate.pickOnly = function(obj, property, keys) {
    obj[property] = _.pick(obj[property], keys);
    return Q(obj);
};

/**
 * Remove tags from specified keys of object
 * @param obj
 * @returns {exports|*}
 */
Validate.sanitize = function(obj, keys) {
    var regex = /(<([^>]+)>)/ig;
    _.forEach(keys, function(key) {
        obj[key] = obj[key].replace ? obj[key].replace(regex, "") : obj[key];
    });
    return Q(obj);
};

/**
 * Must have all specified keys without any exception.
 * @param obj
 * @param keys
 * @returns {*}
 */
Validate.mustHaveKeys = function(obj, keys) {
    try {
        obj.should.have.keys(keys);
        return Q(true);
    } catch (err) {
        return Q.reject(errors.MISSING_REQUIRED_FIELDS({required: keys}));
    }
};

/**
 * Check specified keys for correct type.
 * @param obj
 * @param map {object} - {keyToCheck: correctType}, i.e. {email: String, password: String}
 * @returns {exports|*}
 */
Validate.checkTypes = function(obj, map) {
    var errorKeys = [];
    for(var key in map) {
        if(!maybe(obj[key]).is(map[key]).getOrElse(null)) {
            errorKeys.push(key);
        }
    }
    return errorKeys.length ? Q.reject(errors.NOT_CORRECT_TYPE({keys: errorKeys})) : Q(true);
};

/**
 * Validate password: should have more then 6 chars and more then 1 digit among them.
 * @param password
 * @returns {exports|*}
 */
Validate.password = function(password) {
    var result = maybe(password)
        .is(String)
        .map(function(str) {
            return str.length > 6 ? str : null;
        })
        .map(function(str) {
            return /\D/.test(str) && /\d/.test(str) ? str : null;
        })
        .getOrElse(null);

    return result ? Q(result) : Q.reject(errors.WEAK_PASSWORD());
};

module.exports = Validate;