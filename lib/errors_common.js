module.exports = {
    MISSING_REQUIRED_FIELDS: {
        status: 400,
        code: 99001,
        message: 'Some required fields are missed in request body'
    },
    NOT_CORRECT_TYPE: {
        status: 400,
        code: 99002,
        message: 'Some field has wrong type'
    }
};