var _       = require('lodash');
var log     = require('./logger').getLogger('user');
var Q       = require('q');
var crypto  = require('crypto');
var maybe   = require('maybe2');

var MongoUser = require('./db_mongo_user');
var uid       = require('./uid');
var utils     = require('./mongo_utils');
var errors    = require('./errors');

var User = function(data) {
    if(!data) return null;
    if(data._doc) data = data._doc;
    return _.extend(this, data);
};

/**
 * Find users that matches specified query.
 * @param query - The query to find users.
 * @param options - Options for the query (skip, limit, sort).
 * @returns {Promise|promise.then|*} Array of objects (type of User).
 */
User.get = function(query, options) {
    return Q.nbind(MongoUser.find, MongoUser)(query, options)
        .then(function(users) {
            log.debug("find user: ", users);
            return _.map(users, function(user) {
                return new User(user);
            });
        });
};

/**
 * Find a user that matches specified query.
 * @param query - The query to find a user.
 * @returns {Promise|promise.then|*} An object (type of User).
 */
User.getOne = function(query) {
    return Q.nbind(MongoUser.findOne, MongoUser)(query)
        .then(function(user) {
            log.debug("findOne user: ", user);
            return user ? new User(user) : null;
        });
};

/**
 * Create new user object with specified fields.
 * @param data - Properties to create new user object.
 * @returns {Promise|promise.then|*} An object (type of User).
 */
User.create = function(data) {
    // TODO maybe this method will be changed when new auth strategies are available.
    // maybe we'll use 'provider' code to split the logic
    var duplicatesQuery = utils.makeDuplicatesQuery(data, 'email');

    return Q.nbind(MongoUser.findOne, MongoUser)(duplicatesQuery)
        .then(function(user) {
            if(user) return Q.reject(errors.USER_EMAIL_EXISTS());
            return user;
        })
        .then(function() {
            return uid.next();
        })
        .then(function(nextUid) {
            data.uid = nextUid;
            if(data.email) data.nae = true; // set NotActivatedEmail to true

            var newUser = new User(data);
            return newUser.setEncryptedPassword(data.password);
        })
        .then(function(newUser) {
            return Q.nbind(MongoUser.create, MongoUser)(newUser)
                .then(function(user) {
                    log.debug("New user successfully created");
                    // create activation link with fire-and-forget call if email exists
                    if(user.email) {
                        var EmailActivator = require('./email_activation');
                        EmailActivator.createActivationLink(user);
                    }
                    return new User(user);
                });
        });
};

/**
 * Find the maximum uid in database.
 * @returns {Promise|promise.then|*} The max uid from user collecion (type of Number).
 */
User.getMaxUid = function() {
    return Q.ninvoke(MongoUser.findOne().sort('-uid'), 'exec')
        .then(function(user) {
            var user = new User(user);
            return user ? user.uid : null;
        });
};

/**
 * Save current user entity.
 * @returns {Promise|promise.then|*}
 */
User.prototype.save = function() {
    return Q.nbind(MongoUser.findByIdAndUpdate, MongoUser)(this._id, _.omit(this, '_id'))
        .then(function(user) {
            log.debug('user entity successfully saved');
            return new User(user);
        });
};

/**
 * Change password of a user if current is actual, otherwise reject with error.
 * @param {string} currentPwd - Current password.
 * @param {string} newPwd - New password.
 * @returns {Promise} - Promise.
 */
User.prototype.changePassword = function(currentPwd, newPwd) {
    log.debug("User.prototype.changePassword called", currentPwd, newPwd);
    if(this.authenticate(currentPwd)) {
        return this.setEncryptedPassword(newPwd)
            .then(function(user) {
                return user.save();
            });
    } else {
        return Q.reject(errors.WRONG_CURRENT_PASSWORD());
    }
};

User.update = function() {

};


/**
 * Set hashpass and salt properties to User object.
 * @param password - Valid password
 * @returns {*} - Promise.
 */
User.prototype.setEncryptedPassword = function(password) {
    this.salt = makeSalt();
    this.hashpass = encryptPassword(password, this.salt);
    return Q(this);
};

/**
 * Authenticate User by password phrase.
 * Return TRUE if it is ok and false otherwise.
 * @param password - Password to check.
 * @returns {boolean} - Authentication result.
 */
User.prototype.authenticate = function(password) {
    try {
        return encryptPassword(password, this.salt) == this.hashpass;
    } catch(err) {
        return false;
    }
};


/* Utils for User object */
function makeSalt() {
    return Math.round((new Date().valueOf() * Math.random())) + '';
}

function encryptPassword(password, salt) {
    return crypto.createHmac('sha1', salt).update(password).digest('hex');
}

module.exports = User;