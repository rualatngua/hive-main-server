/**
 * Object with all types of emails and translations of subjects.
 * In mailer module we'll make all these emails callable functions
 * for creating required emails.
 * @type {{passwordRecover: {html: string, text: string, subject: {en: string, ru: string}}}}
 */
module.exports = {
    passwordRecover: {
        html: "password_recover.html",
        text: "password_recover.txt",
        subject: {
            en: "Hive - recover password",
            ru: "Hive - восстановление пароля"
        }
    },
    passwordChanged: {
        html: "password_changed.html",
        text: "password_changed.txt",
        subject: {
            en: "Hive - password changed",
            ru: "Hive - пароль изменён"
        }
    },
    passwordReset: {
        html: "password_reset.html",
        text: "password_reset.txt",
        subject: {
            en: "Hive - password reset",
            ru: "Hive - пароль восстановлен"
        }
    },
    activateEmail: {
        html: "activate_email.html",
        text: "activate_email.txt",
        subject: {
            en: "Hive - activate email",
            ru: "Hive - активируйте адрес электронной почты"
        }
    },
    changeEmail: {
        html: "change_email.html",
        text: "change_email.txt",
        subject: {
            en: "Hive - change email",
            ru: "Hive - смена адреса электронной почты"
        }
    },
    emailChanged: {
        html: "email_changed.html",
        text: "email_changed.txt",
        subject: {
            en: "Hive - email changed",
            ru: "Hive - изменён адрес электронной почты"
        }
    }
};