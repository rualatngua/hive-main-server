var path        = require('path');
var log4js      = require('log4js');
var settings    = require('./settings');

if(settings.env == 'unittest') {
    /**
     * No appenders for unit tests, if you want to debug something just
     * uncomment console.log appender.
     */
    log4js.configure({
        appenders: [
//            {type: 'console', "level": "ALL"} // uncomment this line if you need to see logs in unit tests
        ]
    });
} else {
    log4js.configure(path.join(__dirname, '../conf/log4js_config.json'), {});
}

module.exports = log4js;