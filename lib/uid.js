var Q        = require('q');
var log      = require('./logger').getLogger('uid');
var redis    = require('redis');
var settings = require('./settings');
var client   = redis.createClient(settings.config.redis_port, settings.config.redis_host);

var redisUidKey = settings.env == 'test' ? 'test:uid_autoincrement' : 'uid_autoincrement';

var uid = {};

uid.redisUidKey = redisUidKey;

uid.next = function() {
    return Q.ninvoke(client, 'get', redisUidKey)
        .then(
            function(currentUid) {
                log.debug("try to get current uid from redis", currentUid);
                if(currentUid) {
                    return incrementUidInRedis();
                } else {
                    return getUidFromMongoAndIncrement();
                }
            },
            function(err) {
                log.error("can not do 'get' command", err);
                return Q.reject(err);
            }
        );

    function incrementUidInRedis() {
        return Q.nbind(client.incr, client)(redisUidKey);
    }

    function getUidFromMongoAndIncrement() {
        log.warn("could not find uid_autoincrement in Redis");

        var User = require('./user');
        return User.getMaxUid().then(function(maxUid) {
            log.debug("get max uid from mongodb", maxUid);
            maxUid = maxUid ? maxUid : 0;

            var multi = client.multi();

            multi.set(redisUidKey, maxUid);
            multi.incr(redisUidKey);

            return Q.ninvoke(multi, 'exec')
                .then(function(replies) {
                    return replies[1];
                });
        });
    }

};

module.exports = uid;