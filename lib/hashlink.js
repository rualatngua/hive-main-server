var _       = require('lodash');
var log     = require('./logger').getLogger('hashlink');
var Q       = require('q');
var errors  = require('./errors');
var crypto  = require('crypto');

var redis    = require('redis');
var settings = require('./settings');
var client   = redis.createClient(settings.config.redis_port, settings.config.redis_host);

var idToLinkPrefix = "idlink:";
var linkToIdPrefix = "linkid:";

function idKey(id) {
    return idToLinkPrefix + id;
}

function linkKey(hash) {
    return linkToIdPrefix + hash;
}

function generateHash() {
    return crypto.randomBytes(16).toString('hex');
}

var Hashlink = function(data) {
    if(!data) return null;
    return _.extend(this, data);
};


/**
 * Get user id from db by specified link(hash).
 * @param hash - Hash.
 * @returns {Promise|String} - User id.
 */
Hashlink.get = function(hash) {
    return Q.nbind(client.get, client)(linkKey(hash))
        .then(function(data) {
            if(!data) {
                log.error('such hash link does not exist', hash);
                return Q.reject(errors.HASH_LINK_NOT_FOUND());
            }
            try {
                return JSON.parse(data);
            } catch(err) {
                return data;
            }
        }, function(err) {
            log.error('error on fetching hash link from db', err);
            return Q.reject(errors.GET_USER_ID_FROM_HASH_LINK({err: err}));
        });
};

/**
 * Remove both id->link and link->id pairs from db.
 * @param hash - Link(hash) to remove.
 * @returns {Q.catch|*} - Promise.
 */
Hashlink.remove = function(hash) {
    var multi = client.multi();
    return Q.ninvoke(multi, 'get', linkKey(hash))
        .then(function(id) {
            multi.del(linkKey(hash));
            multi.del(idKey(id));
            return Q.ninvoke(multi, 'exec')
                .then(function(data) {
                    log.debug("remove hash link from redis", data);
                    return data;
                });
        })
        .catch(function(err) {
            log.error('error on removing hash link from db', err);
            return Q.reject(errors.REMOVE_HASH_LINK({err: err}));
        });
};

/**
 * Create link(hash) for specified user id.
 * @param id - Some user id.
 * @returns {Promise|String} - Link(hash).
 */
Hashlink.create = function(id, data) {
    return Q.nbind(client.get, client)(idKey(id))
        .then(function(hash) {
            if(hash) {
                log.warn("attempt to create hash link one more time");
                return Q.reject(errors.HASH_LINK_EXISTS());
            }
            return hash;
        })
        .then(function() {
            var hashLink = generateHash();
            if(data) {
                var dataToStore = JSON.stringify(_.extend({id: id}, data));
            } else {
                dataToStore = id;
            }
            /**
             * We need the ability to find link by user id and user id by link,
             * that is why we create two relations in redis:
             *  + user id    -> link(hash)
             *  + link(hash) -> user id
             *  Expire time is set for both of them.
             */
            var multi = client.multi();
            multi.set(idKey(id), hashLink);
            multi.set(linkKey(hashLink), dataToStore);
            multi.expire(idKey(id), settings.config.hash_link_expire);
            multi.expire(linkKey(hashLink), settings.config.hash_link_expire);

            return Q.ninvoke(multi, 'exec')
                .then(function() {
                    return hashLink;
                })
                .catch(function(err) {
                    log.error('error on creating hash link', err);
                    return Q.reject(errors.HASH_LINK_CREATE({err: err}));
                });
        });
};

module.exports = Hashlink;
