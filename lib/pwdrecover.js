var _    = require('lodash');
var log  = require('./logger').getLogger('pwdrecover');
var Q    = require('q');

var User        = require('./user');
var HashLink    = require('./hashlink');
var errors      = require('./errors');
var mailer      = require('./mailer');
var settings    = require('./settings');
var moment      = require('moment');

var Pwdrecover = {};

/**
 * Create recover link in redis and send it to specified email.
 * @param email - Email to send recover link to.
 * @returns {Q.catch|*} - Promise.
 */
Pwdrecover.createRecoverLink = function(email) {
    return User.getOne({ email: email })
        .then(function(user) {
            log.debug("get user by email", user);
            if(!user) {
                log.warn("no user with such email found");
                return Q.reject(errors.RECOVER_EMAIL_NOT_FOUND());
            }
            return user;
        })
        .then(function(user) {
            return HashLink.create(user._id)
                .then(function(recoverLink) {
                    log.debug("create recover link for user", recoverLink);
                    return {
                        user: user,
                        lang: user.lang || 'en',
                        recoverLink: recoverLink,
                        expires: moment().add('seconds', settings.config.hash_link_expire).fromNow('h')
                    }
                });
        })
        .then(function(data) {
            moment.lang(data.lang);

            return mailer.passwordRecover(data.user.email, data, data.lang)
                .then(function() {
                    log.debug("sent recover email to user");
                    return {
                        status: "ok"
                    };
                });
        })
        .catch(function(err) {
            log.error("error on creating recover link and email", {email: email, err: err});
            return Q.reject(err);
        });
};

/**
 * Set new password to user according to provided recover link.
 * @param data {Object} - {link, password}.
 * @returns {Promise|*} - Promise.
 */
Pwdrecover.resetPassword = function(data) {
    return HashLink.get(data.link)
        .then(function(id) {
            return User.getOne({_id: id})
        })
        .then(function(user) {
            if(!user) {
                return Q.reject(errors.NO_USER_FOR_HASH_LINK());
            }
            return user;
        })
        .then(function(user) {
            return user.setEncryptedPassword(data.password);
        })
        .then(function(user) {
            return user.save();
        })
        .then(function(user) {
            HashLink.remove(data.link);
            mailer.passwordReset(user.email, {user: user}, user.lang);
            return user;
        })
};

module.exports = Pwdrecover;