var passport = require("passport");
var _        = require("lodash");
var log      = require("./logger").getLogger('api_auth');

var errors   = require("./errors");

var express = require("express");
var app = module.exports = express();

/* rules for serialize sessions (we need them as common part) */
passport.serializeUser(function(user, done) {
    var id = user.id || user._id;
    log.debug("passport serializeUser", id);
    done(null, id);
});

/*
 * We are not going to deserialize user for each request.
 * If you need user entity you can get it from DB by id.
 */
passport.deserializeUser(function(id, done) {
    log.debug("passport deserializeUser", id);
    done(null, id);
});

/* add ensureAuthenticated method to passport */
_.extend(passport, {
    ensureAuthenticated: function(req, res, next) {
        if(req.isAuthenticated()) {
            log.debug("user is authenticated");
            return next();
        } else {
            log.warn("user is not authenticated", {cookie: req.cookies['connect.sid'], session: req.session});
            return next(errors.USER_NOT_AUTHENTICATED());
        }
    }
});

/* initialize passport itself and passport sessions */
app.use(passport.initialize());
app.use(passport.session());

/* mount required passport strateges */
app.use(require("./auth_local"));

app.get('/logout', function(req, res) {
    log.debug("logout user");
    if(req.user) {
        req.logout();
    }
    res.json();
});