/*
 * Errors with user starts from 10***
 */
module.exports = {
    USER_EMAIL_EXISTS: {
        status: 409,
        code: 10001,
        message: 'User with such email already exists'
    },
    USER_NOT_AUTHENTICATED: {
        status: 403,
        code: 10002,
        message: 'Forbidden for not authorized users'
    },
    USER_AUTH_FAILD: {
        status: 403,
        code: 10003,
        message: 'Authentication faild, user not found'
    },
    USER_LOGIN_FAILD: {
        status: 403,
        code: 10004,
        message: 'Login faild'
    },
    NO_USER_WITH_EMAIL: {
        status: 403,
        code: 10005,
        message: 'No user with such email'
    },
    NOT_VALID_EMAIL_OR_PASSWORD: {
        status: 403,
        code: 10006,
        message: 'Not valid pair email/password'
    },
    WEAK_PASSWORD: {
        status: 400,
        code: 10007,
        message: 'Weak password'
    },
    WRONG_CURRENT_PASSWORD: {
        status: 400,
        code: 10008,
        message: 'Current password is wrong'
    },
    NO_USER_TO_ACTIVATE_EMAIL: {
        status: 404,
        code: 10009,
        message: 'User for email activation not found'
    },
    NO_USER_TO_CHANGE_EMAIL: {
        status: 404,
        code: 10010,
        message: 'User for email change not found'
    },
    WRONG_PASSWORD_ON_EMAIL_CHANGE: {
        status: 400,
        code: 10011,
        message: 'Password was wrong on attempt to change email'
    },
    CURRENT_EMAIL_NEEDS_ACTIVATION: {
        status: 400,
        code: 10012,
        message: 'If you want to change your email you have to activate it first'
    }
};