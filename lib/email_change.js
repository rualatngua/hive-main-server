var _    = require('lodash');
var log  = require('./logger').getLogger('email_change');
var Q    = require('q');

var User     = require('./user');
var errors   = require('./errors');
var mailer   = require('./mailer');
var moment   = require('moment');
var HashLink = require('./hashlink');
var settings = require('./settings');
var EmailActivator = require('./email_activation');

var EmailChanger = {};

EmailChanger.createChangeLink = function(user, newEmail, password) {
    if(!user.authenticate(password)) {
        log.error('wrong password while changing email', {_id: user._id});
        return Q.reject(errors.WRONG_PASSWORD_ON_EMAIL_CHANGE());
    }

    // check that current user's email is activated
    if(user.nae) {
        // TODO for now we just send error to ask user to activate his current email first
        log.warn("change email of user with not activated email", {_id: user._id, email: user.email});
        return Q.reject(errors.CURRENT_EMAIL_NEEDS_ACTIVATION());
    } else {
        return HashLink.create(user._id, {email: newEmail})
            .then(function(emailCahangeLink) {
                var data = {
                    emailChangeLink: emailCahangeLink,
                    user: user,
                    expires: moment().add('seconds', settings.config.hash_link_expire).fromNow('h')
                };
                return mailer.changeEmail(user.email, data, user.lang);
            });
    }
};

EmailChanger.changeEmail = function(link) {
    var HashData;
    return HashLink.get(link)
        .then(function(hashData) {
            HashData = hashData;
            return User.getOne({_id: HashData.id})
        })
        .then(function(user) {
            if(!user) {
                log.error('user not found for email change');
                return Q.reject(errors.NO_USER_TO_CHANGE_EMAIL());
            }
            return user;
        })
        .then(function(user) {
            var originEmail = user.email;
            user.email = HashData.email;

            return user.save()
                .then(function(savedUser) {
                    mailer.emailChanged(originEmail, {user: user}, user.lang);
                    EmailActivator.createActivationLink(savedUser);
                    return savedUser;
                });
        });
};

module.exports = EmailChanger;