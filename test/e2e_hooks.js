process.env.NODE_ENV = 'test';
var exec     = require('child_process').exec;
var spawn    = require('child_process').spawn;
var request  = require('request');
var util     = require('util');
var settings = require('../lib/settings');
var _        = require('lodash');
var fs       = require('fs');
var async    = require('async');
var appProcess;
var smtpProcess;

function line() { var line = ''; while(line.length < 50){ line+='*'} return line;}

console.log(line(), '\n* PREPARE TEST ENVIRONMENT');

function dropDB(callback) {
    var db_name = settings.config.db_name;
    var command = util.format("mongo %s --eval 'db.dropDatabase()'", db_name);
    setTimeout(function() {
        exec(command, function(err, stdout, stderr) {
            console.log("* remove test database ");
            callback ? callback() : null;
        })
    }, 10);
}

function startSMTP(port, output) {
    if(!port || !output) {
        throw {
            name: "Bad config",
            message: "Config file should contain port and output params for mail_server!"
        };
    }
    console.log("* start SMTP server on port: ", port);
    smtpProcess = spawn('python', ['./test/smtp_sink_server.py', '--port', port, '--output', output]);
}

function killProcess() {
    if(smtpProcess) {
        console.log("* kill smtp process");
        smtpProcess.kill();
        smtpProcess = null;
    }
    if (appProcess) {
        console.log("* kill app process");
        console.log("* END UP TEST SESSION\n", line());
        appProcess.kill();
        appProcess = null;
    }
}

var before = function(done) {
    this.timeout(5000);

    try {
        fs.lstatSync("./logs");
    } catch(err) {
        if(err.code == 'ENOENT') {
            console.log("* ./logs dir was created for test logs");
            fs.mkdirSync("./logs");
        }
    }

    appProcess = spawn('node', ['app.js'], {env: _.extend(process.env, { NODE_ENV: 'test' })});

    /**
     * Kill the appProcess after test finished
     */
    process.on('exit', killProcess);

    /**
     * Waiting for server and drop db
     */
    function waitForServer(callback) {
        var url = util.format("http://localhost:%d/", settings.config.port);
        var interval = setInterval(function() {
            request(url, function(err, res, body) {
                console.log("* * check server: ", err, body);
                if(res && res.statusCode == 200) {
                    console.log('* now server is running\n', line(), '\n\n\n');
                    clearInterval(interval);
                    callback();
                }
            });
        }, 500);
    }

    async.waterfall([
        function(cb) { dropDB(cb); },
        function(cb) {startSMTP(settings.config.mail_server.port, settings.config.mail_server.output); cb();},
        function(cb) { waitForServer(cb); }
    ], done);
};

var after = function(done) {
    async.waterfall([
        function(cb) { dropDB(cb); },
        function(cb) { killProcess(); cb(); }
    ], done);
};

module.exports = {
    before: before,
    after: after
};