var hooks = require('../e2e_hooks');
require('should');
var Modules = require('../modules');

var headers = {
    'Content-Type': 'application/json',
    'user-agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B141 Safari/8536.25'
};
var request = require('request').defaults({
    jar: true,
    headers: headers
});

var config = require(Modules.settings).config;

function url(url) {
    return "http://localhost:" + config.port + "/api" + url;
}

describe('lib.api_user', function() {

    before(hooks.before);
    after(hooks.after);

    describe('POST /users', function() {

        it('should properly create user', function(done) {
            var user = {
                email: "zahar@lodossteam.com",
                password: "qwerty123"
            };
            request({
                url: url("/users"),
                method: "POST",
                body: JSON.stringify(user)
            }, function(err, res, body) {
                res.statusCode.should.be.equal(200);
                body = JSON.parse(body);
                body.should.have.keys(['accessToken']);
                done();
            });
        });

        it('should return logged user entity', function(done) {
            request({
                url: url("/users"),
                method: "GET"
            }, function(err, res, body) {
                res.statusCode.should.be.equal(200);
                body = JSON.parse(body);
                body.should.have.properties([
                    '_id',
                    'uid',
                    'country',
                    'agent',
                    'email',
                    'created',
                    'friends'
                ]);
                body.agent.should.be.equal('iOS 6.1');
                done();
            });
        });

        it('should return 409 status code on attempt to create user with the same email', function(done) {
            var user = {
                email: "zahar@lodossteam.com",
                password: "qwerty123"
            };
            request({
                url: url("/users"),
                method: "POST",
                body: JSON.stringify(user)
            }, function(err, res, body) {
                res.statusCode.should.be.equal(409);
                body = JSON.parse(body);
                body.code.should.be.type('number');
                done();
            });
        });

        it('should properly create user with password', function(done) {
            var user = {
                email: "max@lodossteam.com",
                password: "qwerty123"
            };
            request({
                url: url("/users"),
                method: "POST",
                body: JSON.stringify(user)
            }, function(err, res, body) {
                res.statusCode.should.be.equal(200);
                body = JSON.parse(body);
                body.should.have.keys(['accessToken']);
                done();
            });
        });

    });

    describe('POST /auth/local', function() {

        it('should login user', function(done) {
            var query = {
                email: "max@lodossteam.com",
                password: "qwerty123"
            };
            request({
                url: url("/auth/local"),
                method: "POST",
                body: JSON.stringify(query)
            }, function(err, res, body) {
                res.statusCode.should.be.equal(200);
                done();
            });
        });

        it('should change user password', function(done) {
            var query = {
                current: "qwerty123",
                password: "123qwerty"
            };
            request({
                url: url("/change/pwd"),
                method: "POST",
                body: JSON.stringify(query)
            }, function(err, res, body) {
                res.statusCode.should.be.equal(200);
                done();
            });
        });

    });

});