process.env.NODE_ENV = 'unittest';
require('should');
var _       = require('lodash');
var Q       = require('q');
var sinon   = require('sinon');
var Modules = require('../modules');
var proxyquire =  require('proxyquire');

describe('lib.user', function() {

    var User = require(Modules.user);

    describe('User', function() {

        it('should properly create user', function() {
            var user = new User({
                name: 'zahar',
                email: 'zahar@lodossteam.com'
            });

            user.should.eql({
                name: 'zahar',
                email: 'zahar@lodossteam.com'
            });
        });

    });

    describe('User.create', function() {

        it('should call MongoUser function .create() and properly initialize data', sinon.test(function(done) {
            var user = {
                name: 'zahar',
                email: 'zahar@lodossteam.com',
                password: 'qwerty123'
            };

            var User = proxyquire(Modules.user, {
                './uid': {
                    next: function() { return Q(10); }
                },
                './db_mongo_user': {
                    create: function() {
                        return _.last(arguments)(null, _.extend({_id: 1}, user));
                    },
                    findOne: function() {
                        return _.last(arguments)(null, null);
                    }
                }
            });

            User.create(user).then(function(user) {
                user.should.eql({
                    _id: 1,
                    uid: 10,
                    name: 'zahar',
                    email: 'zahar@lodossteam.com',
                    password: 'qwerty123',
                    nae: true
                });
                user.should.be.an.instanceof(User);
            }).done(done);
        }));

    });

    describe('User.getOne', function() {

        it('should call MongoUser function .findOne() and properly initialize data', sinon.test(function(done) {
            var MongoUser = require(Modules.MongoUser);
            var stub = this.stub(MongoUser, 'findOne').withArgs({_id: 1}).yields(null, {
                _id: 1,
                name: 'zahar',
                email: 'zahar@lodossteam.com'
            });

            User.getOne({_id: 1}).then(function(user) {
                user.should.eql({
                    _id: 1,
                    name: 'zahar',
                    email: 'zahar@lodossteam.com'
                });
                user.should.be.an.instanceof(User);
            }).done(done);
        }));

    });

    describe('User.get', function() {

        it('should call MongoUser function .find() and properly initialize array of data', sinon.test(function(done) {
            var MongoUser = require(Modules.MongoUser);
            var stub = this.stub(MongoUser, 'find').withArgs({active: true}, undefined).yields(null, [
                {
                    _id: 1,
                    name: 'zahar',
                    email: 'zahar@lodossteam.com'
                },
                {
                    _id: 2,
                    name: 'zahar2',
                    email: 'zahar2@lodossteam.com'
                }
            ]);

            User.get({active: true}).then(function(users) {
                users.should.be.an.instanceof(Array);
                users[0].should.be.an.instanceof(User);
                users[0].should.eql({
                    _id: 1,
                    name: 'zahar',
                    email: 'zahar@lodossteam.com'
                });
            }).done(done);
        }));

        it('should call MongoUser function .find() and return empty array', sinon.test(function(done) {
            var MongoUser = require(Modules.MongoUser);
            var stub = this.stub(MongoUser, 'find').withArgs({active: true}, undefined).yields(null, []);

            User.get({active: true}).then(function(users) {
                users.should.be.an.instanceof(Array);
                users.length.should.be.equal(0);
            }).done(done);
        }));

    });

    describe('User.getMaxUid', function() {

        var getFakeFindOneResult = function(fakeErr, fakeRes) {
            return {
                sort: function() {
                    return {
                        exec: function(cb) {
                            return cb(fakeErr, fakeRes);
                        }
                    }
                }
            }
        };

        it('should return undefined', sinon.test(function(done) {
            var MongoUser = require(Modules.MongoUser);
            var stub = this.stub(MongoUser, 'findOne');
            stub.returns(getFakeFindOneResult(null, null));

            User.getMaxUid().then(function(res) {
                (res == undefined).should.be.ok;
            }).done(done);
        }));

        it('should return max number', sinon.test(function(done) {
            var MongoUser = require(Modules.MongoUser);
            var stub = this.stub(MongoUser, 'findOne');
            stub.returns(getFakeFindOneResult(null, {uid: 3001}));

            User.getMaxUid().then(function(res) {
                res.should.be.equal(3001);
            }).done(done);
        }));

        it('should return error if something bad', sinon.test(function(done) {
            var MongoUser = require(Modules.MongoUser);
            var stub = this.stub(MongoUser, 'findOne');
            stub.returns(getFakeFindOneResult({error: 'error'}, null));

            User.getMaxUid().then(null, function(err) {
                err.should.be.ok;
            }).done(done);
        }));

    });

    describe('User.prototype.setEncryptedPassword', function() {

        it('should properly set salt and hashed password', function() {
            var data = {
                name: "zahar",
                password: "qwerty123"
            };
            var user = new User(data);

            user.setEncryptedPassword(data.password);
            user.should.have.property('salt');
            user.should.have.property('hashpass');
        });

    });

    describe('User.prototype.authenticate', function() {

        it('should return true for right password', function() {
            var data = {
                name: "zahar",
                password: "qwerty123"
            };
            var user = new User(data);

            user.setEncryptedPassword(data.password);

            var authResult = user.authenticate("qwerty123");
            authResult.should.be.ok;
        });

        it('should return false for wrong password', function() {
            var data = {
                name: "zahar",
                password: "qwerty123"
            };
            var user = new User(data);

            user.setEncryptedPassword(data.password);

            var authResult = user.authenticate("qwerty12");
            authResult.should.not.be.ok;
        });

        it('should return false for empty password', function() {
            var data = {
                name: "zahar",
                password: "qwerty123"
            };
            var user = new User(data);

            user.setEncryptedPassword(data.password);

            var authResult = user.authenticate();
            authResult.should.not.be.ok;
        });

    });

});