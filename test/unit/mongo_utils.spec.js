process.env.NODE_ENV = 'unittest';
var should  = require('should');

var Modules = require('../modules');

describe('lib.mongo_utils', function() {

    var userUtils = require(Modules.mongoUtils);

    describe('makeDuplicatesQuery', function() {

        it('should create query with only email', function() {
            var user = {
                name: 'zahar',
                email: 'zahar@lodossteam.com'
            };

            var query = userUtils.makeDuplicatesQuery(user, 'email');
            query.should.eql({
                $or: [{email: 'zahar@lodossteam.com'}]
            });
        });

        it('should create query with email and fb_id', function() {
            var user = {
                name: 'zahar',
                email: 'zahar@lodossteam.com',
                fb_id: 1234
            };

            var query = userUtils.makeDuplicatesQuery(user, 'email', 'fb_id');
            query.should.eql({
                $or: [{email: 'zahar@lodossteam.com'}, {fb_id: 1234}]
            });
        });

    });

});