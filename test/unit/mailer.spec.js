process.env.NODE_ENV = 'unittest';
require('should');
var Q = require('q');
var _ = require('lodash');
var proxyquire =  require('proxyquire');

var Modules = require('../modules');
var errors = require(Modules.errors);

describe('lib.mailer', function() {

    var mailer = proxyquire(Modules.mailer, {
        'emailjs': {
            server: {
                connect: function() {
                    return {
                        send: function(mail) {
                            return _.last(arguments)(null, mail);
                        }
                    }
                }
            }
        }
    });

    describe('mailer', function() {

        it('should return compiled and rendered templates', function(done) {
            var data = {
                user: {
                    name: "Zahar A."
                },
                recoverLink: "some_long_unique_hash",
                expires: "an hour"
            };
            var to = "zahar@lodossteam.com";

            mailer.passwordRecover(to, data, 'en').then(function(sentMail) {
                sentMail.should.have.keys([
                    'to',
                    'from',
                    'text',
                    'subject',
                    'attachments'
                ]);
            }).done(done);
        });

        it('should return compiled and rendered templates in Russian language', function(done) {
            var data = {
                user: {
                    name: "Zahar A."
                },
                recoverLink: "some_long_unique_hash",
                expires: "час"
            };
            var to = "zahar@lodossteam.com";

            mailer.passwordRecover(to, data, 'ru').then(function(sentMail) {
                sentMail.should.have.keys([
                    'to',
                    'from',
                    'text',
                    'subject',
                    'attachments'
                ]);
                sentMail.subject.should.include('восстановление пароля');
                sentMail.text.should.include('Уважаемый пользователь');
            }).done(done);
        });

        it('should return error while language is not correct', function(done) {
            var data = {
                user: {
                    name: "Zahar A."
                },
                recoverLink: "some_long_unique_hash",
                expires: "hour"
            };
            var to = "zahar@lodossteam.com";

            mailer.passwordRecover(to, data, 'ololo').then(function(sentMail) {
                'success'.should.equal('error');
            }, function(err) {
                err.should.be.ok;
                err.code.should.equal(errors.EMAIL_RENDER().code);
            }).done(done);
        });

        it('should return error while error happens on sending', function(done) {
            var mailer = proxyquire(Modules.mailer, {
                'emailjs': {
                    server: {
                        connect: function() {
                            return {
                                send: function(mail) {
                                    return _.last(arguments)({error: 'error'});
                                }
                            }
                        }
                    }
                }
            });

            var data = {
                user: {
                    name: "Zahar A."
                },
                recoverLink: "some_long_unique_hash",
                expires: "hour"
            };
            var to = "zahar@lodossteam.com";

            mailer.passwordRecover(to, data, 'en').then(function(sentMail) {
                'success'.should.equal('error');
            }, function(err) {
                err.should.be.ok;
                err.code.should.be.equal(errors.COULD_NOT_SEND_EMAIL().code);
            }).done(done);
        });

    });

});