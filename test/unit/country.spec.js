process.env.NODE_ENV = 'unittest';
require('should');

var Modules = require('../modules');

describe('lib.country', function() {

    var country = require(Modules.country);

    describe('country', function() {

        it('should return null for localhost', function() {
            var code = country.getByIp('127.0.0.1');
            (code == null).should.be.ok;
        });

        it('should return 2-char counrty code for normal ip', function() {
            var code = country.getByIp('8.8.8.8');
            code.should.be.equal('US');
        });

        it('should return null if no ip', function() {
            var code = country.getByIp();
            (code == null).should.be.ok;
        });

        it('should return null if not valid ip', function() {
            var code = country.getByIp("ololo");
            (code == null).should.be.ok;
        });

    });

});