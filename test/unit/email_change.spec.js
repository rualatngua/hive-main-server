process.env.NODE_ENV = 'unittest';
require('should');
var Q = require('q');
var _ = require('lodash');
var proxyquire =  require('proxyquire');

var Modules = require('../modules');
var errors  = require(Modules.errors);

describe('lib.email_change', function() {

    describe('EmailChanger.createChangeLink', function() {

        var EmailChanger = proxyquire(Modules.email_change, {
            './mailer': {
                changeEmail: function(to, data) {
                    return Q(data);
                }
            }
        });

        it('should create email-change link and send email to user', function(done) {
            var User = function() {
                _.extend(this, {_id: '528cad6552064f426fee196b', email: 'zahar@lodossteam.com'});
            };
            User.prototype.authenticate = function() {
                return true;
            };

            var newEmail = 'zahar@lodoss.org';
            var password = 'qwerty123';

            EmailChanger.createChangeLink(new User(), newEmail, password)
                .then(function(data) {
                    data.should.have.properties([
                        'emailChangeLink',
                        'user'
                    ]);
                    data.emailChangeLink.length.should.be.equal(32);
                }, function(err) {
                    'error'.should.be.equal('success');
                }).done(done);
        });

        it('should return error when current email not activated', function(done) {
            var User = function() {
                _.extend(this, {_id: '528cad6552064f426fee196b', nae:true, email: 'zahar@lodossteam.com'});
            };
            User.prototype.authenticate = function() {
                return true;
            };

            var newEmail = 'zahar@lodoss.org';
            var password = 'qwerty123';

            EmailChanger.createChangeLink(new User(), newEmail, password)
                .then(function(data) {
                    'success'.should.be.equal('error');
                }, function(err) {
                    err.code.should.be.equal(errors.CURRENT_EMAIL_NEEDS_ACTIVATION().code);
                }).done(done);
        });

        it('should return error when password is wrong', function(done) {
            var User = function() {
                _.extend(this, {_id: '528cad6552064f426fee196b', nae:true, email: 'zahar@lodossteam.com'});
            };
            User.prototype.authenticate = function() {
                return false;
            };

            var newEmail = 'zahar@lodoss.org';
            var password = 'qwerty123';

            EmailChanger.createChangeLink(new User(), newEmail, password)
                .then(function(data) {
                    'success'.should.be.equal('error');
                }, function(err) {
                    err.code.should.be.equal(errors.WRONG_PASSWORD_ON_EMAIL_CHANGE().code);
                }).done(done);
        });

    });

    describe('EmailChanger.changeEmail', function() {

        it('should change email with right link and password', function(done) {
            var EmailChanger = proxyquire(Modules.email_change, {
                './user': {
                    getOne: function(query) {
                        var User = function() {
                            _.extend(this, {email: 'zahar@lodossteam.com', name: 'Zahar A.'}, query);
                        };
                        User.prototype.save = function() {
                            return Q(this);
                        };
                        return Q(new User());
                    }
                },
                './email_activation': {
                    createActivationLink: function() {}
                },
                './hashlink': {
                    get: function() {
                        return Q({id:"some_user_id",email:"zahar@lodoss.org"});
                    }
                }
            });

            EmailChanger.changeEmail('some_hash_link')
                .then(function(user) {
                    user.email.should.be.equal('zahar@lodoss.org');
                }, function(err) {
                    'error'.should.be.equal('success');
                }).done(done);
        });

        it('should return error if user not found from link', function(done) {
            var EmailChanger = proxyquire(Modules.email_change, {
                './user': {
                    getOne: function(query) {
                        return Q(null);
                    }
                },
                './hashlink': {
                    get: function() {
                        return Q({id:"some_user_id",email:"zahar@lodoss.org"});
                    }
                }
            });

            EmailChanger.changeEmail('some_hash_link')
                .then(function() {
                    'success'.should.be.equal('error');
                }, function(err) {
                    err.code.should.be.equal(errors.NO_USER_TO_CHANGE_EMAIL().code);
                }).done(done);
        });

    });

});