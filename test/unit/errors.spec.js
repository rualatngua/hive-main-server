process.env.NODE_ENV = 'unittest';
require('should');
var _ = require('lodash');

var Modules = require('../modules');

describe('lib.errors', function() {

    var errors = require(Modules.errors);

    describe('errors', function() {

        it('should contain only functions', function() {
            _.forEach(errors, function(value) {
                value.should.be.type('function');
            });
        });

        it('should return object with predefined fields', function() {
            var someError = _.toArray(errors)[0];
            var errorObj = someError();
            errorObj.should.be.type('object');
            errorObj.should.have.keys([
                'status',
                'code',
                'message',
                'timestamp'
            ]);
        });

        it('should extends default object with provided values', function() {
            var someError = _.toArray(errors)[0];
            var errorObj = someError({
                message: 'New message',
                fatal: 'yes'
            });
            errorObj.fatal.should.be.equal('yes');
            errorObj.message.should.be.equal('New message');
        });

    });

});