process.env.NODE_ENV = 'unittest';
var should  = require('should');

var Modules = require('../modules');
var errors  = require(Modules.errors);

describe('lib.validate', function() {

    var V = require(Modules.validate);

    describe('pickOnly', function() {

        it('should fetch only specified properties', function(done) {
            var req = {
                body: {
                    email: 'zahar@lodossteam.com',
                    password: 'qwerty123',
                    wrong: 123
                }
            };
            V.pickOnly(req, 'body', ['email', 'password']).then(function() {
                req.body.should.have.keys([
                    'email',
                    'password'
                ]);
            }, function() {
                'error'.should.be.equal('success');
            }).done(done);
        });

    });

    describe('mustHaveKeys', function() {

        it('should be ok if all properties are defined', function(done) {
            var req = {
                body: {
                    email: 'zahar@lodossteam.com',
                    password: 'qwerty123'
                }
            };
            V.mustHaveKeys(req.body, ['email', 'password']).then(function() {
                'success'.should.be.ok;
            }, function() {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should throw error if not all properties are defined', function(done) {
            var req = {
                body: {
                    email: 'zahar@lodossteam.com'
                }
            };
            V.mustHaveKeys(req.body, ['email', 'password']).then(function() {
                'success'.should.be.equal('error');
            }, function(error) {
                error.code.should.be.equal(errors.MISSING_REQUIRED_FIELDS().code);
            }).done(done);
        });

        it('should throw error if empty object', function(done) {
            var req = {
                body: {}
            };
            V.mustHaveKeys(req.body, ['email', 'password']).then(function() {
                'success'.should.be.equal('error');
            }, function(error) {
                error.code.should.be.equal(errors.MISSING_REQUIRED_FIELDS().code);
            }).done(done);
        });

    });

    describe('checkTypes', function() {

        it('should be ok if types are correct', function(done) {
            var req = {
                body: {
                    string: 'zahar@lodossteam.com',
                    number: 1234,
                    object: {},
                    array: [],
                    boolean: true
                }
            };
            V.checkTypes(req.body, {
                'string': String,
                'number': Number,
                'object': Object,
                'array': Array,
                'boolean': Boolean
            }).then(function() {
                'success'.should.be.ok;
            }, function(error) {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should throw error if types are not correct', function(done) {
            var req = {
                body: {
                    string: 123,
                    number: 'string',
                    object: 123,
                    array: 123,
                    boolean: 123
                }
            };
            V.checkTypes(req.body, {
                'string': String,
                'number': Number,
                'object': Object,
                'array': Array,
                'boolean': Boolean
            }).then(function() {
                'success'.should.be.equal('error');
            }, function(error) {
                error.code.should.be.equal(errors.NOT_CORRECT_TYPE().code);
                error.keys.should.include('string');
                error.keys.should.include('number');
                error.keys.should.include('object');
                error.keys.should.include('array');
                error.keys.should.include('boolean');
            }).done(done);
        });

    });

    describe('password', function() {

        it('should be ok for strong password', function(done) {
            var password = 'qwerty123';
            V.password(password).then(function() {
                'success'.should.be.ok;
            }, function() {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should return error if password is weak', function(done) {
            var password = 'qwerty';
            V.password(password).then(function() {
                'success'.should.be.equal('error');
            }, function(error) {
                error.code.should.be.equal(errors.WEAK_PASSWORD().code);
            }).done(done);
        });

        it('should return error if password is not defiend', function(done) {
            var password;
            V.password(password).then(function() {
                'success'.should.be.equal('error');
            }, function(error) {
                error.code.should.be.equal(errors.WEAK_PASSWORD().code);
            }).done(done);
        });

    });
});