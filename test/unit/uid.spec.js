process.env.NODE_ENV = 'unittest';
var should  = require('should');
var Q       = require('q');
var _       = require('lodash');
var sinon   = require('sinon');
var proxyquire =  require('proxyquire');

var Modules = require('../modules');
var uidPath = Modules.uid;

describe('lib.uid', function() {

    describe('UID', function() {

        it('should call next() and return 11', sinon.test(function(done) {
            var UID = proxyquire(uidPath, {
                'redis': {
                    createClient: function() {
                        return {
                            get: function() {
                                return _.last(arguments)(null, 10);
                            },
                            incr: function() {
                                return _.last(arguments)(null, 11);
                            }
                        }
                    }
                }
            });

            UID.next().then(function(res) {
                res.should.be.equal(11);
            }).done(done);
        }));

        it('should return error on redis .get()', sinon.test(function(done) {
            var UID = proxyquire(uidPath, {
                'redis': {
                    createClient: function() {
                        return {
                            get: function() {
                                return _.last(arguments)({error: 'error'}, null);
                            }
                        }
                    }
                }
            });

            UID.next().then(function(res) {
                'success'.should.equal('error');
            }, function(err) {
                err.should.be.ok;
            }).done(done);
        }));

        it('should return error on redis .incr()', sinon.test(function(done) {
            var UID = proxyquire(uidPath, {
                'redis': {
                    createClient: function() {
                        return {
                            get: function() {
                                return _.last(arguments)(null, 10);
                            },
                            incr: function() {
                                return _.last(arguments)({error: 'error'}, null);
                            }
                        }
                    }
                }
            });

            UID.next().then(function(res) {
                'success'.should.equal('error');
            }, function(err) {
                err.should.be.ok;
                err.error.should.be.equal('error');
            }).done(done);
        }));

        it('should return error on User .getMaxUid() while tying to find max uid in mongo', sinon.test(function(done) {
            var UID = proxyquire(uidPath, {
                'redis': {
                    createClient: function() {
                        return {
                            get: function() {
                                return _.last(arguments)(null, null);
                            }
                        }
                    }
                },
                './user': {
                    getMaxUid: function() {
                        return Q.reject({error: 'error'});
                    }
                }
            });

            UID.next().then(function(res) {
                'success'.should.equal('error');
            }, function(err) {
                err.should.be.ok;
                err.error.should.be.equal('error');
            }).done(done);
        }));

        it('should return error on redis .multi() while tying to set new uid', sinon.test(function(done) {
            var UID = proxyquire(uidPath, {
                'redis': {
                    createClient: function() {
                        return {
                            get: function() {
                                return _.last(arguments)(null, null);
                            },
                            multi: function() {
                                return {
                                    set: function() { return {}; },
                                    incr: function() { return {}; },
                                    exec: function() {
                                        return _.last(arguments)({error: 'error'}, null);
                                    }
                                }
                            }
                        }
                    }
                },
                './user': {
                    getMaxUid: function() {
                        return Q(10);
                    }
                }
            });

            UID.next().then(function(res) {
                'success'.should.equal('error');
            }, function(err) {
                err.should.be.ok;
                err.error.should.be.equal('error');
            }).done(done);
        }));

        it('should get max uid from mongo, set it to redis and return', sinon.test(function(done) {
            var UID = proxyquire(uidPath, {
                'redis': {
                    createClient: function() {
                        return {
                            get: function() {
                                return _.last(arguments)(null, null);
                            },
                            multi: function() {
                                return {
                                    set: function() { return {}; },
                                    incr: function() { return {}; },
                                    exec: function() {
                                        return _.last(arguments)(null, ['ok', 11]);
                                    }
                                }
                            }
                        }
                    }
                },
                './user': {
                    getMaxUid: function() {
                        return Q(10);
                    }
                }
            });

            UID.next().then(function(res) {
                res.should.be.equal(11);
            }, function(err) {
                'error'.should.equal('success');
            }).done(done);
        }));
    });

});