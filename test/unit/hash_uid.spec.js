process.env.NODE_ENV = 'unittest';
require('should');

var Modules = require('../modules');

describe('lib.hash_uid', function() {

    var hashUid = require(Modules.hashUid);

    describe('hash_uid', function() {

        it('should generate 6 chars hash from a digit', function() {
            var hash = hashUid.getHash(1);
            hash.should.be.type('string');
            hash.length.should.be.equal(6);
            hash.should.be.equal('47VQDQ');
        });

        it('should generate 6 chars hash from a long digit', function() {
            var hash = hashUid.getHash(5000000);
            hash.length.should.be.equal(6);
            hash.should.be.equal('JVNPPZ');
        });

        it('should get uid from 6 chars hash', function() {
            var hash = '47VQDQ';
            var uid = hashUid.getIdFromHash(hash);
            uid.should.be.type('number');
            uid.should.be.equal(1);
        });

        it('should generate the same hash each time', function() {
            var uid = 2;
            var hash1 = hashUid.getHash(uid);
            var hash2 = hashUid.getHash(uid);
            hash1.should.be.equal('875K7G');
            hash2.should.be.equal('875K7G');
        });

    });

});