REPORTER = spec

unit:
		@./node_modules/.bin/mocha \
			--reporter $(REPORTER) \
			--ui bdd \
			--bail \
			test/unit/*.spec.js

e2e:
		@./node_modules/.bin/mocha \
			--reporter $(REPORTER) \
			--ui bdd \
			--bail \
			test/e2e/*.spec.js

all: unit e2e

.PHONY: all